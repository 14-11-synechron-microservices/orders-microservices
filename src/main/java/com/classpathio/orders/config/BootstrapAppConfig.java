package com.classpathio.orders.config;

import static java.util.stream.IntStream.range;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.classpathio.orders.model.LineItem;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.repository.OrderRepository;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * To insert fake data for us to test
 * Useful during unit testing and integration testing
 * 
 * @author pradeep
 *
 */
@Component
@RequiredArgsConstructor
@Log4j2
public class BootstrapAppConfig  {//implements ApplicationListener<ApplicationReadyEvent>{

	private final OrderRepository orderRepository;
	private final Faker faker = new Faker();
	@Value("${app.totalOrders}")
	private int totalOrders;
	
	@EventListener(value = ApplicationReadyEvent.class)
	public void onApplicationEvent(ApplicationReadyEvent event) {
		log.info("Loading application data::::");
		range(0, totalOrders).forEach(index -> {
			//create order and save to the DB
			String customerName = faker.name().firstName();
			Order order = Order.builder()
								.name(customerName)
								.email(customerName+ "@"+faker.internet().domainName())
								.orderDate(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
								.build();
			range(0, faker.number().numberBetween(2, 5)).forEach(value -> {
				LineItem lineItem = LineItem.builder()
												.name(faker.commerce().productName())
												.qty(faker.number().numberBetween(2, 5))
												.price(faker.number().randomDouble(2, 400, 800))
											.build();
				
				//bind both sides of the relationship
				/*
				 * order.getLineItems().add(lineItem); lineItem.setOrder(order);
				 */
				//set the total order price
				order.addLineItem(lineItem);
				double orderPrice = order
										.getLineItems()
										.stream()
										.map(lI -> lI.getQty() * lI.getPrice())
										.reduce(Double::sum)
										.orElse(0d);
				order.setPrice(orderPrice);
				
			});
			this.orderRepository.save(order);
		});
	}

}
