package com.classpathio.orders.service;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpathio.orders.event.OrderEvent;
import com.classpathio.orders.event.OrderStatus;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.repository.OrderRepository;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {
	
	private final OrderRepository orderRepository;
	
	private final WebClient webClient;
	
	private final StreamBridge streamBridge;
	
	@CircuitBreaker(name="inventoryservice", fallbackMethod="fallBack")
	@Transactional
	public Order saveOrder(Order order) {
		//we will call the inventory microservice
		//Hit the post endpoint http://inventory-microservice-svc/api/inventory
		/*
		 * long orderCount = this.webClient.post() .uri("/api/inventory") .retrieve()
		 * .bodyToMono(Long.class) .block();
		 */
		//use the message broker to send the message
		
		//System.out.println("Response from the inventory microservice :: "+ orderCount);
		
		//commit the changes to the database
		Order savedOrder = this.orderRepository.save(order);
		//Create an event with the updated order
		OrderEvent orderPlacedEvent = new OrderEvent(savedOrder, OrderStatus.ORDER_ACCEPTED, LocalDateTime.now());
		// create a Payload
		Message<OrderEvent> orderEventPayload = MessageBuilder.withPayload(orderPlacedEvent).build();
		this.streamBridge.send("producer-out-0", orderEventPayload);
		return savedOrder;
	}
	
	private Order fallBack(Exception exception) {
		System.out.println("Exception while communicating with the inventory microservice :: "+ exception.getMessage());
		return Order.builder().build();
	}
	
	public Map<String, Object> fetchAllOrders(int page, int size, String strDirection, String field){
		Sort.Direction direction = strDirection.equalsIgnoreCase("asc") ? Sort.Direction.ASC: Sort.Direction.DESC;
		Pageable pageRequest = PageRequest.of(page, size, direction, field);
		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
		
		long totalRecords = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		long currentSize = pageResponse.getSize();
		List<Order> data = pageResponse.getContent();
		
		Map<String, Object> response = new LinkedHashMap<>();
		response.put("records", totalRecords);
		response.put("pages", totalPages);
		response.put("size", currentSize);
		response.put("data", data);
		return response;
	}
	
	public Order fetchOrderByOrder(long orderId) {
		return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}

}
