package com.classpathio.orders.controller;

import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.service.OrderService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	
	private final OrderService orderService;
	
	@GetMapping
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name = "page", defaultValue = "0", required = false) int page, 
			@RequestParam(name = "size", defaultValue = "5", required = false) int size, 
			@RequestParam(name = "order", defaultValue = "asc", required = false) String direction, 
			@RequestParam(name = "field", defaultValue = "name", required = false) String field){
		return this.orderService.fetchAllOrders(page, size, direction, field);
	}
	
	@GetMapping("/{id}")
	public Order fetchOrders(@PathVariable("id") long orderId){
		return this.orderService.fetchOrderByOrder(orderId);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order) {
		return this.orderService.saveOrder(order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable("id") long orderId) {
		this.orderService.deleteOrderById(orderId);
	}

}
