package com.classpathio.orders.controller;

import static org.springframework.boot.availability.AvailabilityChangeEvent.publish;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/state")
@RequiredArgsConstructor
public class StateRestController {

	private final ApplicationAvailability applicationAvailability;
	private final ApplicationEventPublisher applicationEvent;

	@PostMapping("/liveness")
	public Map<String, Object> updateLiveness() {
		// toggle the state
		LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
		LivenessState updatedLivenessState = currentLivenessState == LivenessState.CORRECT ? LivenessState.BROKEN
				: LivenessState.CORRECT;
		String state = updatedLivenessState == LivenessState.CORRECT ? "System is functioning"
				: "Application is not functioning";

		Map<String, Object> responseMap = new LinkedHashMap<>();

		// publish the change event
		publish(applicationEvent, state, updatedLivenessState);

		responseMap.put("liveness", updatedLivenessState);
		responseMap.put("state", state);
		return responseMap;
	}

	@PostMapping("/readiness")
	public Map<String, Object> updateReadiness() {
		ReadinessState readinessState = this.applicationAvailability.getReadinessState();
		ReadinessState updatedReadinessState = readinessState == ReadinessState.ACCEPTING_TRAFFIC ? ReadinessState.REFUSING_TRAFFIC
				: ReadinessState.ACCEPTING_TRAFFIC;

		String state = updatedReadinessState == ReadinessState.ACCEPTING_TRAFFIC ? "System is functioning"
				: "Application is not functioning";

		// publish the change event
		publish(applicationEvent, state, updatedReadinessState);

		Map<String, Object> responseMap = new LinkedHashMap<>();
		responseMap.put("readiness", updatedReadinessState);
		responseMap.put("state", state);
		return responseMap;

	}

}
