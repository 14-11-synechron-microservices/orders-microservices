package com.classpathio.orders.event;

import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventHandler {
	
	@EventListener(AvailabilityChangeEvent.class)
	public void handleApplicationCHangeEvent(AvailabilityChangeEvent event) {
		System.out.println("Change in the application event :: "+event);
	}

}
