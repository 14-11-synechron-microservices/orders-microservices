package com.classpathio.orders.event;

public enum OrderStatus {
	ORDER_ACCEPTED,
	ORDER_FULFILLED,
	ORDER_CANCELLED,
	ORDER_REJECTED
}
