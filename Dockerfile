# steps to build the docker image
#layer-0 openjdk-11 image
FROM openjdk:11-jdk-slim as builder

# COPY the dependencies to the docker image
WORKDIR /app

#copy source to destination
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

#Install all the dependencies to the docker image
RUN chmod +x ./mvnw && ./mvnw -B dependency:go-offline


COPY src src
RUN ./mvnw package -DskipTests

RUN mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)


# brand new image
FROM openjdk:11.0.13-jre-slim-buster as stage
ARG DEPENDENCY=/app/target/dependency
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app

# debugging with curl
RUN apt update && apt install -y curl
EXPOSE 8222

ENTRYPOINT [ "java",  "-cp", "app:app/lib/*", "com.classpathio.orders.OrdersMicroserviceApplication"]
